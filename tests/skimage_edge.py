from skimage import io, filters, feature
import matplotlib.pyplot as plt
from skimage.color import rgb2gray
import cv2 as cv
import numpy as np

img = cv.imread('../data/stacks240/stack-1/01560279753918.jpg')
# cv.imshow("original", img)
gray_img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
# cv.imshow('gray', np.array(gray_img, dtype = np.uint8))
gray = np.array(gray_img, dtype = np.uint8)

# processed_img = cv.cvtColor(gray_img, cv.COLOR_RGB2BGR)


#Edge detection
from skimage.filters import roberts, sobel, scharr, prewitt, farid

# roberts_img = roberts(gray)
# sobel_img = sobel(gray_img)
# scharr_img = scharr(gray_img)    
# prewitt_img = prewitt(gray_img)
# farid_img = farid(gray_img)

# cv.imshow("Roberts", roberts_img)
# cv.imshow("Sobel", sobel_img)
# cv.imshow("Scharr", scharr_img)
# cv.imshow("Prewitt", prewitt_img)
# cv.imshow("Farid", farid_img)
# cv.waitKey(0)
# cv.destroyAllWindows()

prw_mag = np.sqrt(sum([prewitt(gray_img, axis=i)**2 for i in range(gray_img.ndim)]) / gray_img.ndim)
print(prw_mag)