import random
from dom import DOM
import cv2 as cv
import sys
import os
from PIL import Image
import numpy as np

def preprocess_img(img):
    img = Image.open(img)
    ori_w = img.size[0]
    ori_h = img.size[1]
    
    left = (ori_w - (ori_w / 2)) / 2
    top = (ori_h - (ori_h / 2)) / 2
    right = (ori_w + (ori_w / 2)) / 2
    bottom = (ori_h + (ori_h / 2)) / 2

    centered_img = img.crop((left, top, right, bottom))

    resized_w = int(centered_img.size[0] * 0.1)
    resized_h = int(centered_img.size[1] * 0.1)
    resized_img = centered_img.resize((resized_w, resized_h))
    return resized_img

def get_sharpness(img):
    preprocessed_img = preprocess_img(img)
    opencvImage = cv.cvtColor(np.array(preprocessed_img), cv.COLOR_RGB2BGR)
    iqa = DOM()
    score = iqa.get_sharpness(opencvImage)

    return score

random.seed(0)

stack = []
for i in range(150):
    stack.append(random.randint(0, 200))

    stack.sort()

right_wing = []
for i in range(150):
    right_wing.append(random.randint(0, 200))
    right_wing.sort(reverse=True)


for i in right_wing:
    stack.append(i)

# print(stack)


# def stride(stack):
#     print(len(stack))
#     highest = 0 
#     if len(stack) % 3 == 0:
#         one_third = int(len(stack) / 3)
#         two_thirds = int(len(stack) / 3 * 2)

#         first = stack[:one_third]
#         third = stack[two_thirds:]
#         second = stack[one_third:two_thirds]
#     direction = 'positive'
#     # step = 1

#     print(second)

#     for i, img in enumerate(second):
#         if direction == 'positive':
#             if second[i] <= second[i+1]:
#                 highest_value = second[i+1]
#                 continue
#             else:
#                 return highest_value

def stride(stack):
    print(len(stack))
    highest = 0 
    if len(stack) % 3 == 0:
        one_third = int(len(stack) / 3)
        two_thirds = int(len(stack) / 3 * 2)

        first = stack[:one_third]
        third = stack[two_thirds:]
        second = stack[one_third:two_thirds]
    # direction = 'positive'
    # step = 1

    print(second)

    highest_value = 0
    for i, img in enumerate(second):
        # if direction == 'positive':
            current_img = get_sharpness(second[i])
            next_img = get_sharpness(second[i+1])

            if current_img <= next_img:
                if highest_value <= next_img:
                    highest_value = next_img
                else:
                    continue
            else:
                for i, img in enumerate(third):
                # if direction == 'positive':
                    current_img = get_sharpness(third[i])
                    try:
                        next_img = get_sharpness(third[i+1])
                    except IndexError:
                        break

                    if current_img <= next_img:
                        if highest_value <= next_img:
                            highest_value = next_img
                        else:
                            continue
                    else:
                        for i, img in enumerate(first):
                        # if direction == 'positive':
                            current_img = get_sharpness(first[len(first)-i-1])
                            next_img = get_sharpness(first[len(first)-i-2])

                            if current_img <= next_img:
                                if highest_value <= next_img:
                                    highest_value = next_img
                                else:
                                    continue
                            else:
                                break
    return img, highest_value
        
# print(stride(stack))

# def stride2(stack):
#     print(len(stack))
#     highest = 0 
#     if len(stack) % 3 == 0:
#         one_third = int(len(stack) / 3)
#         two_thirds = int(len(stack) / 3 * 2)

#         first = stack[:one_third]
#         third = stack[two_thirds:]
#         second = stack[one_third:two_thirds]
#     direction = 'positive'
#     # step = 1

#     print(second)

#     start_pos = int(len(second)/2)

#     highest_not_found = True
#     left_ok = False
#     right_ok = False

#     while highest_not_found:
#         if direction == 'positive':
#             step = 0
#             if second[start_pos + step] <= second[start_pos + step + 1]:
#                 highest_value = second[start_pos + step + 1]
#                 step += 1

#                 if (start_pos + step + 1) > len(second):
#                     start_pos = int(len(third)/2)
#                     if direction == 'positive':
#                         step = 0
#                         if third[start_pos + step] <= third[start_pos + step + 1]:
#                             highest_value = third[start_pos + step + 1]
#                             step += 1
#                         else:
#                             direction = 'negative'
#                             right_ok = True

#                     else:
#                         step = 0
#                         if third[start_pos - step] <= third[start_pos - step - 1]:
#                             highest_value = third[start_pos - step - 1]
#                             step += 1
#                             continue
#                         else:
#                             left_ok = True                    
#             else:
#                 direction = 'negative'
#                 right_ok = True
#         else:
#             step = 0
#             if second[start_pos - step] <= second[start_pos - step - 1]:
#                 highest_value = second[start_pos - step - 1]
#                 step += 1
#                 continue
#             else:
#                 left_ok = True

#         if right_ok and left_ok:
#             highest_not_found = False
    
#     return highest_value

# print(stride2(stack))

if __name__ == '__main__':
    data_files = 'data/stacks240/stack-1'
    test_dict = dict()
    img_files = []
    for img_file_name in os.listdir(data_files):
        img_file = os.path.join(data_files, img_file_name)
        # img_files.append(img_file)
        img_files.append(img_file)
    
    print(stride(img_files))