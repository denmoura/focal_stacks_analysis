import random
from dom import DOM
import cv2 as cv
import sys
import os
from PIL import Image
import numpy as np
import math

golden_ratio = (3 - math.sqrt(5))/2

def preprocess_img(img):
    img = Image.open(img)
    ori_w = img.size[0]
    ori_h = img.size[1]
    
    left = (ori_w - (ori_w / 2)) / 2
    top = (ori_h - (ori_h / 2)) / 2
    right = (ori_w + (ori_w / 2)) / 2
    bottom = (ori_h + (ori_h / 2)) / 2

    centered_img = img.crop((left, top, right, bottom))

    resized_w = int(centered_img.size[0] * 0.1)
    resized_h = int(centered_img.size[1] * 0.1)
    resized_img = centered_img.resize((resized_w, resized_h))

    # resized_w = int(ori_w * 0.1)
    # resized_h = int(ori_h * 0.1)

    # resized_img = img.resize((resized_w, resized_h))

    return resized_img

def get_sharpness(img):
    preprocessed_img = preprocess_img(img)
    opencvImage = cv.cvtColor(np.array(preprocessed_img), cv.COLOR_RGB2BGR)
    iqa = DOM()
    score = iqa.get_sharpness(opencvImage)

    return score

searching = True

def golden_section_search(l, m, r, f, stack):
    global golden_ratio
    global searching
    
    if searching == True:
        N = m + (r - m)*golden_ratio
        if (r - m) < (m - l):
            N = l + (m - l)*golden_ratio
        N = round(N)

        f_N = f(stack[int(N)])
        f_m = f(stack[int(m)])
        print(N, f_N, f_m)
        if f_N == f_m:
            return stack[int(N)], f_N

        elif f_N > f_m:
            if N > m:
                print('looping here')
                golden_section_search(m, N, r, f, stack)
                
            else:
                print('looping here2')
                golden_section_search(l, N, m, f, stack)
                
        elif f_N < f_m:
            if N > m:
                print('looping here3')
                golden_section_search(l, m, N, f, stack)
                
            elif m >= N:
                print('looping here4')
                golden_section_search(N, m, r, f, stack)

        else:
            searching = False
            return stack[int(N)], f_N           

    return stack[int(N)], f_N


### PORCARIA DE LOOP INFINITO!!!1

if __name__ == '__main__':
    data_files = 'data/stacks240/stack-1'
    test_dict = dict()
    img_files = []
    for img_file_name in os.listdir(data_files):
        img_file = os.path.join(data_files, img_file_name)
        # img_files.append(img_file)
        img_files.append(img_file)
    
    best_focus, score = golden_section_search(0, len(img_files)/2, len(img_files) - 1, get_sharpness, img_files)

    print("The best focused picture is: \n", best_focus, "\n with a score of ", score)