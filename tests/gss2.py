import random
import sys
import os
from PIL import Image
import numpy as np
import math
import cv2 as cv
from dom import DOM

def preprocess_img(img):
    img = Image.open(img)
    ori_w = img.size[0]
    ori_h = img.size[1]
    
    left = (ori_w - (ori_w / 2)) / 2
    top = (ori_h - (ori_h / 2)) / 2
    right = (ori_w + (ori_w / 2)) / 2
    bottom = (ori_h + (ori_h / 2)) / 2

    centered_img = img.crop((left, top, right, bottom))

    resized_w = int(centered_img.size[0] * 0.1)
    resized_h = int(centered_img.size[1] * 0.1)
    resized_img = centered_img.resize((resized_w, resized_h))
    return resized_img

def get_sharpness(img):
    preprocessed_img = preprocess_img(img)
    opencvImage = cv.cvtColor(np.array(preprocessed_img), cv.COLOR_RGB2BGR)
    iqa = DOM()
    score = iqa.get_sharpness(opencvImage)

    return score

random.seed(0)

stack = []
for i in range(150):
    stack.append(random.randint(0, 200))

    stack.sort()

right_wing = []
for i in range(150):
    right_wing.append(random.randint(0, 200))
    right_wing.sort(reverse=True)


for i in right_wing:
    stack.append(i)

def golden_section_search(l, m, h, f, stack):
    golden_ratio = (3 - math.sqrt(5))/2

    searching = True
    while searching:
        N = m + (h - m) * golden_ratio
        if (h - m) < (m - l):
            N = l + (m - l) * golden_ratio
        N = round(N)

        f_N = f(stack[int(N)])
        f_m = f(stack[int(m)])

        print(N, f_N, f_m)

        if f_N == f_m:
            return "end"
        
        elif f_N > f_m:
            if N > m:
                print('loop1')
                golden_section_search(m, N, h, f, stack)

            else:
                print('loop2')
                golden_section_search(l, N, m, f, stack)
        elif f_N < f_m:
            if N > m:
                print("loop3")
                golden_section_search(l, m, N, f, stack)
            
            elif m >= N:
                print("loop4")
                golden_section_search(N, m, h, f, stack)
            
        else:
            searching = False
            return stack[int(N)], N, stack[int(m)], m

    return stack[int(N)], N, stack[int(m)], m


if __name__ == '__main__':
    data_files = 'data/stacks240/stack-1'
    test_dict = dict()
    img_files = []
    for img_file_name in os.listdir(data_files):
        img_file = os.path.join(data_files, img_file_name)
        # img_files.append(img_file)
        img_files.append(img_file)
    
    print(golden_section_search(0, len(img_files)/2, len(img_files) - 1, get_sharpness, img_files))