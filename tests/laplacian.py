import cv2

def variance_of_laplacian(img):
    return cv2.Laplacian(img, cv2.CV_64F).var()