import random

random.seed(0)

stack = []
for i in range(150):
    stack.append(random.randint(0, 200))

    stack.sort()

right_wing = []
for i in range(150):
    right_wing.append(random.randint(0, 200))
    right_wing.sort(reverse=True)


for i in right_wing:
    stack.append(i)

print(stack)

def getLarger(a,b):
    if a < b:
        return b
    else:
        return a

def getSmaller(a,b):
    if a > b:
        return b
    else:
        return a
max = 10
min = 0

def divide_and_conquer(stack, start, end):
    # if len(stack) // 2 == 0:
    #     print('impaired', len(stack))
    # else:
    #     print('paired', len(stack))
    # first_half = stack[:int(len(stack)/2)]
    # second_half = stack[int(len(stack)/2):]

    # print(first_half)
    # print(second_half)
    global max
    global min
    mid = int((start + end) / 2)
    if end - start > 2:
        divide_and_conquer(stack, start, mid)
        divide_and_conquer(stack, mid, end)
    
    else:
        stack = stack[start:end]
        if end - start == 1:
            stack.append(stack[0])
        max = getLarger(max, getLarger(stack[0], stack[1]))
        min = getSmaller(min, getSmaller(stack[0], stack[1]))

    return max, min


print(divide_and_conquer(stack, 0, len(stack)))