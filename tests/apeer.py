from dom import DOM
import cv2 as cv
import sys
import os
from PIL import Image
import numpy as np

def preprocess_img(img):
    img = Image.open(img)
    ori_w = img.size[0]
    ori_h = img.size[1]
    
    left = (ori_w - (ori_w / 2)) / 2
    top = (ori_h - (ori_h / 2)) / 2
    right = (ori_w + (ori_w / 2)) / 2
    bottom = (ori_h + (ori_h / 2)) / 2

    centered_img = img.crop((left, top, right, bottom))

    resized_w = int(centered_img.size[0] * 0.1)
    resized_h = int(centered_img.size[1] * 0.1)
    resized_img = centered_img.resize((resized_w, resized_h))
    return resized_img

def get_sharpness(img):
    preprocessed_img = preprocess_img(img)
    opencvImage = cv.cvtColor(np.array(preprocessed_img), cv.COLOR_RGB2BGR)
    iqa = DOM()
    score = iqa.get_sharpness(opencvImage)

    return score

if __name__ == '__main__':
    #data_files = sys.argv[1]
    data_files = 'data/stacks240/stack-1'
    test_dict = dict()
    img_files = []
    for img_file_name in os.listdir(data_files):
        img_file = os.path.join(data_files, img_file_name)
        # img_files.append(img_file)
        img_files.append(img_file)
        test_dict[img_file_name] = get_sharpness(img_file)
        # print(get_sharpness(img_file))
    sorted_dict = {k: v for k, v in sorted(test_dict.items(), key=lambda item: item[1])}
    print(sorted_dict)
    print(sorted_dict['01560279754802.jpg'])

    # focusing = True
    # while focusing:
    #     best_sharpness = 0
    #     for i in range(len(img_files)):
    #         curr_sharpness = get_sharpness(img_files[i])

    #         if curr_sharpness > best_sharpness:
    #             best_sharpness = curr_sharpness
    #             best_focus = img_files[i]
    #         else:
    #             continue

    #         if i == len(img_files):
    #             focusing = False

    # print(best_focus, best_sharpness)