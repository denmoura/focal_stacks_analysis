import imutils
import cv2
import sys

#def preprocess_img(image):
#    img = cv2.imread(image, 1)
#    img = imutils.resize(img, width = 300)

    # cv2.imshow("Normal", img)
    # cv2.waitKey(0)
#    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # cv2.imshow("Gray", gray)

    # edged = cv2.Canny(gray, 40, 120)
    # cv2.imshow("Edged", edged)
    # cv2.waitKey(0)

    #thresh = cv2.threshold(gray, 75, 255, cv2.THRESH_BINARY_INV)[1]
    # cv2.imshow("Thresh", thresh)
    # cv2.waitKey(0)

    # cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
    #     cv2.CHAIN_APPROX_SIMPLE)
    # cnts = imutils.grab_contours(cnts)
    # output = img.copy()

    # for c in cnts:
    #     cv2.drawContours(output, [c], -1, (240, 0, 159), 3)
        
    # cv2.imshow("Contours", output)
    # cv2.waitKey(0)

    #mask = thresh.copy()
    #mask = cv2.erode(mask, None, iterations = 1)
    # cv2.imshow("Eroded", mask)

    #output = cv2.bitwise_and(img, img, mask=mask)
    # cv2.imshow("Output", output)

    # cv2.waitKey(0)
    #return output

# if __name__ == '__main__':
#     preprocess_img(sys.argv[1])


from PIL import Image

def preprocess_img(img):
    img = Image.open(img)
    ori_w = img.size[0]
    ori_h = img.size[1]
    
    left = (ori_w - (ori_w / 2)) / 2
    top = (ori_h - (ori_h / 2)) / 2
    right = (ori_w + (ori_w / 2)) / 2
    bottom = (ori_h + (ori_h / 2)) / 2

    centered_img = img.crop((left, top, right, bottom))

    resized_w = int(centered_img.size[0] * 0.1)
    resized_h = int(centered_img.size[1] * 0.1)
    resized_img = centered_img.resize((resized_w, resized_h))

    # resized_w = int(ori_w * 0.1)
    # resized_h = int(ori_h * 0.1)

    # resized_img = img.resize((resized_w, resized_h))

    return resized_img