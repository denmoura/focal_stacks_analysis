# This feature aims to calculate the best focused image from a set of images.
# It tests different image quality measurements
# And different searching algorithms to optimize processing time.

# Usage: The program receives as arguments
# 1) Path to the focal stack (relative to the autofocus.py file)
# 2) Image quality analysis method
    # Currently there are:
    #     FastFourier
    #     Laplacian
    #     RingDiff
    #     JpegSize
    #     Sharpness
    #     BRISQUE
# 3) Optimizer algorithm
    # These can be:
    #     BB = Bounded Brent
    #     BF = Brute Force
    #     GSS = Golden Section Search
# Example:
# python autofocus.py 'data/stacks240/stack-1' Laplacian BB

# We will need libraries for directory control and arguments
import sys
import os

# To reduce computation steps, we preprocess the image using Pillow (PIL)
# And OpenCV (cv2)
# These steps are implemented in preprocess file.
from preprocess import preprocess_img

# We start by trying sharpness evaluation using Pydom algorithm (github.com/umang-singhal/pydom)
## pip pydom
# These steps are implemented in sharpness.py
from sharpness import get_sharpness

# Now we are importing other image quality measurements from other sources
from laplacian import variance_of_laplacian
from gabs import ringDiff, jpegSize, fastFourier
from brisque import brisque_score

# Finally, we are using our implementation of golden section search to quickly find the best focused image
from gss import golden_section_search

# To test runtime, we are also importing time.
import time

# Finally, we're importing pyplot from matplotlib to show the images.
from skimage import color, io
import matplotlib.pyplot as plt
from bounded_brent import bounded_brent_optimizer

start_time = time.time()

if __name__ == '__main__':
    # data_files = 'data/stacks240/stack-1'
    data_files = sys.argv[1]
    img_files = []
    for img_file_name in os.listdir(data_files):
        img_file = os.path.join(data_files, img_file_name)
        # img_files.append(img_file)
        img_files.append(img_file)

    measurements = dict()
    measurements['FastFourier'] = fastFourier
    measurements['Laplacian'] = variance_of_laplacian
    measurements['RingDiff'] = ringDiff
    measurements['JpegSize'] = jpegSize
    measurements['Sharpness'] = get_sharpness
    measurements['BRISQUE'] = brisque_score

    MEASUREMENT = measurements[sys.argv[2]]

    optimizers = dict()
    optimizers['GSS'] = golden_section_search
    optimizers['BB'] = bounded_brent_optimizer
    optimizers['BF'] = 'Brute Force'

    OPTIMIZER = optimizers[sys.argv[3]]

    if len(sys.argv) >= 4:
        CORRECT_FOCUS = data_files + '/' + sys.argv[4]

    if OPTIMIZER != "Brute Force":            
        # best_focus, score = OPTIMIZER(0, len(img_files)/2, len(img_files) - 1, get_sharpness, img_files)
        print('Running {} algorithm with '.format(sys.argv[3]), sys.argv[2], ' as measurement.')
        best_focus, score = OPTIMIZER(0, len(img_files), MEASUREMENT, img_files, sys.argv[2])
        print("The best focused picture is: \n", best_focus, "\n with a score of ", score)
        
        runtime = time.time() - start_time
        print("This program ran in ", runtime, " seconds.")


    if OPTIMIZER == 'Brute Force':
        res = dict()
        print('Running bruteforce algorithm with ', sys.argv[2], ' as measurement.')

        count = 0
        zero_percent = True
        twenty_five_percent = True
        fifty_percent = True
        seventy_five_percent = True

        print("Calculating...")
        for img in img_files:
            count += 1
            # preprocessed_img = preprocess_img(img)
            res[img] = MEASUREMENT(img)
            sorted_dict = {k: v for k, v in sorted(res.items(), key=lambda item: item[1])}
            loading = count / len(img_files)

            if loading < 0.25 and zero_percent:
                print("0%...")
                zero_percent = False

            elif loading >= 0.25 and twenty_five_percent:
                print("25%...")
                twenty_five_percent = False
            
            elif loading >= 0.50 and fifty_percent:
                print("50%...")
                fifty_percent = False

            elif loading >= 0.75 and seventy_five_percent:
                print("75%...")
                seventy_five_percent = False
            
            #print(sorted_dict)
        best_focus = [key for key in sorted_dict.keys()][-1]
        # best_focus = [key for key in sorted_dict.keys()][0] # For BRISQUE
        # print(sorted_dict)
        score = sorted_dict[best_focus]
        print("The best focused picture is: \n", best_focus, "\n with a score of ", score)

        if CORRECT_FOCUS:
            start_position = list(res.keys()).index(CORRECT_FOCUS)

            score2 = sorted_dict[CORRECT_FOCUS]

            position = list(sorted_dict.keys()).index(CORRECT_FOCUS)
            print("The correct focus is image {} in {} position, with a score of {}."
            .format(CORRECT_FOCUS, position, score2))
            print("The image is in the {}% percentile of the focal stack".format(
                start_position/len(res) * 100
            ))

        runtime = time.time() - start_time
        print("This program ran in ", runtime, " seconds.")


        best_img = io.imread(best_focus)
        plt.imshow(best_img)
        plt.show()
