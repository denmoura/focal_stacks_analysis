import cv2 as cv
import numpy as np

def fastFourier(img):
    # img = cv.imread(img)
    h, w, _ = img.shape
    y = np.fft.fft2(img)
    
    output = np.abs(np.absolute(y) * np.angle(y))
    return output.sum() / (h * w)

def jpegSize(img):
    # img = cv.imread(img)
    _, byte_array = cv.imencode('.jpg', img)
    return byte_array.shape[0]


def ringDiff(img):
    # img = cv.imread(img)
    kernel = np.array([[0, 1, 1, 1, 0],
                        [1, 0, 0, 0, 1],
                        [1, 0, -12, 0, 1],
                        [1, 0, 0, 0, 1],
                        [0, 1, 1, 1, 0]])
    
    return cv.filter2D(img, -1, kernel).var()