import math

golden_ratio = (3 - math.sqrt(5))/2

searching = True

def golden_section_search(l, r, f, stack):
    global golden_ratio
    global searching
    
    m = r / 2

    if searching == True:
        N = m + (r - m)*golden_ratio
        if (r - m) < (m - l):
            N = l + (m - l)*golden_ratio
        N = round(N)

        f_N = f(stack[int(N)])
        f_m = f(stack[int(m)])
        # print(N, f_N, f_m)
        if f_N == f_m:
            return stack[int(N)], f_N

        elif f_N > f_m:
            if N > m:
                # print('looping here')
                golden_section_search(m, N, r, f, stack)
                
            else:
                # print('looping here2')
                golden_section_search(l, N, m, f, stack)
                
        elif f_N < f_m:
            if N > m:
                # print('looping here3')
                golden_section_search(l, m, N, f, stack)
                
            elif m >= N:
                # print('looping here4')
                golden_section_search(N, m, r, f, stack)

        else:
            searching = False
            return stack[int(N)], f_N           

    return stack[int(N)], f_N