import cv2 as cv
import numpy as np

def variance_of_laplacian(img):
    # img = cv.imread(img)
    gray_img = cv.cvtColor(np.array(img), cv.COLOR_BGR2GRAY)

    return cv.Laplacian(gray_img, cv.CV_64F).var()