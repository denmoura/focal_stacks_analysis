import cv2
import pytesseract
import argparse
import os

## Argparser

ap = argparse.ArgumentParser()
ap.add_argument("-f", "--folder", required=False,
    help = 'Path to the folder to scan')
ap.add_argument("-i", "--image", required=False,
    help = "Path to a specific image file.")
ap.add_argument("-roi", "--region_of_interest", required=False, type=str,
    help = "A string of comma separated values for Xmin, Xmax, Ymin and Ymax coordinates to define the region of interest.")

args = vars(ap.parse_args())

# Z_AXIS_AREA = (590, 600), (630, 650)
# img = cv2.imread('test.jpeg')
# cv2.imshow('test', img)
# cv2.waitKey(0)

# Z_AXIS_AREA = img[600:650, 590:630]

# cv2.imshow("Z_AXIS", Z_AXIS_AREA)
# cv2.waitKey(0)

def define_roi(img):
    # roi = img[600:670, 590:630]
    roi = img[y_min:y_max, x_min:x_max]

    return roi

def get_z_axis(img):
    Z_AXIS_AREA = define_roi(img)
    z_axis = pytesseract.image_to_string(Z_AXIS_AREA)
    z_axis = z_axis.strip()[:-1]
    
    if len(z_axis) > 5:
        items = z_axis.split(' ')
        for item in items:
            if len(item) > 4:
                z_axis = item
                characters = item.split()
                for i, c in enumerate(characters):
                    if c.isdigit():
                        if characters[i+2] == '.':
                            z_axis = ''.join([c, c+1, c+2, c+3, c+4])
    
    if len(z_axis) == 4:
        if all(c.isdigit() for c in z_axis):
            z_axis = z_axis[0] + z_axis[1] + '.' + z_axis[2] + z_axis[3]

    return z_axis

if __name__ == '__main__':
    
    if args["region_of_interest"]:
        coords = args["region_of_interest"].split(",")
        if len(coords) != 4:
            print("The coordinates are not correctly formatted.")
            exit()

        x_min = int(coords[0])
        x_max = int(coords[1])
        y_min = int(coords[2])
        y_max = int(coords[3])
           
    else:
        y_min, y_max, x_min, x_max = [600, 625, 590, 630]

    if args["image"]:
        img = cv2.imread(args["image"])
        z_axis = get_z_axis(img)
        print(z_axis)

    if args["folder"]:
        folder_path = args["folder"]
        stack_name = folder_path.split('/')[-1]
        
        img_files = []
        for img_file_name in os.listdir(folder_path):
            img_file = os.path.join(folder_path, img_file_name)
            img_files.append(img_file)
        img_files = sorted(img_files)

        z_axes = []
        duplicates = []

        print("Attempting to automatically extract the z axis from the files in {}.".format(stack_name))
        for img_name in img_files:
            img = cv2.imread(img_name)
            z_axis = get_z_axis(img)

            if z_axis in z_axes:
                duplicates.append(img_name)
                pass

            else:
                z_axes.append(z_axis)
            # z_axes.append(z_axis)

        if duplicates:
            print("Duplicates were identified.")
            # print(duplicates)
            remove_duplicates = input("Remove duplicates? y/n \n")
            if remove_duplicates == 'y':
                print("Removing duplicates...")
                for img_file in duplicates:
                    os.remove(img_file)
            
            file_name = "{}_duplicates.txt".format(stack_name)

            with open(file_name, "x") as f:
                for dup in duplicates:
                    f.write("{} ".format(dup))
                f.close()

        try:
            file_name = "{}_z_axes.txt".format(stack_name)
            with open(file_name, "x") as f:
                for z_axis in z_axes:
                    f.write("{}\n".format(z_axis))
                f.close()
            print("Succesfully registered Z axes values for {} in {} file.".format(stack_name, file_name))
        
        except:
            print("File already exists. Cannot overwright")

