import sys

if __name__ == '__main__':
    z_axes_files = sys.argv[1]
    
    with open(z_axes_files, 'r') as f:
        z_axes = []
        for line in f:
            z_axes.append(line.split(' '))
    f.close()

    z_axes = z_axes[0][:-1]

    print("The file length is {}.".format(len(z_axes)))
    dupe_count = []

    for z in z_axes:
        if z in dupe_count:
            print(z)
        dupe_count.append(z)            
