### Extraindo manualmente dados do eixo Z
## Os arquivos da stacks focais não contém metadados para extração automática dos valores do eixo Z.
## Dessa forma, foi necessário extrair manualmente, de cada imagem, o valor do eixo Z.

# Stacks 10x
import numpy as np

focourina1_10x = []
for i in np.arange(16.00, 18.72, 0.02):
    focourina1_10x.append(round(i,2))

remove_list_1 = [ 16.76, 18.44 ]

for i in remove_list_1:
    focourina1_10x.remove(i)

remove_list_2 = [
    16.62, 16.82, 17.92, 18.02, 18.04, 18.08, 18.12, 18.16,
    18.20, 18.24, 18.28, 18.32, 18.36, 18.40, 18.44, 18.48
]

focourina2_10x = []
for i in np.arange(16.60, 18.52, 0.02):
    focourina2_10x.append(round(i,2))

for i in remove_list_2:
    focourina2_10x.remove(i)


# Stacks 40x

focourina1_40x = []
for i in np.arange(16.04, 18.20, 0.01):
    focourina1_40x.append(round(i,2))

remove_list = [
    16.23, 16.86, 17.02, 17.41, 17.76, 17.89, 17.90, 18.10,
]

for i in remove_list:
    focourina1_40x.remove(i)

focourina1_40x.append(18.20)




print(focourina1_40x)
print(len(focourina1_40x))


