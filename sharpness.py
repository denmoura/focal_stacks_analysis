from dom import DOM
import cv2 as cv
import numpy as np
from preprocess import preprocess_img

def get_sharpness(img):
    # preprocessed_img = preprocess_img(img)
    opencvImage = cv.cvtColor(np.array(img), cv.COLOR_RGB2BGR)
    iqa = DOM()
    score = iqa.get_sharpness(opencvImage)

    return score