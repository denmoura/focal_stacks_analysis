import numpy as np
from skimage import io, img_as_float
from skimage.transform import resize
import imquality.brisque as brisque
from skimage.metrics import peak_signal_noise_ratio
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)


def brisque_score(image):
    # img = img_as_float(io.imread(image, as_gray=True))
    img = img_as_float(image)
    img = resize(img, (img.shape[0] // 4, img.shape[1] // 4),
        anti_aliasing=False)

    score = brisque.score(img)
    return score