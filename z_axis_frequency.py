import sys
import os
from sharpness import get_sharpness
from laplacian import variance_of_laplacian
from gabs import ringDiff, jpegSize, fastFourier
from brisque import brisque_score
from preprocess import preprocess_img

BASE_DIR = 'data/'
stacks240 = os.path.join(BASE_DIR, 'stacks240/')
stacks160 = os.path.join(BASE_DIR, 'stacks160/')

real_focus_dict = {'stack-2': '01560279863809.jpg',
    'stack-3': '01560279977204.jpg', 'stack-4': '01560280090599.jpg',
    'stack-5': '01560280206413.jpg', 'stack-7': '01560270248419.jpg',
    'stack-8': '01560270484974.jpg', 'stack-9': '01560270628081.jpg',
    'stack-10': '01560270867665.jpg', 'stack-13': '01560271096589.jpg',
    'stack-14': '01560271168130.jpg', 'stack-16': '01560271326730.jpg',
    'stack-17': '01560271407288.jpg', 'stack-20': '01560271641218.jpg',
    'stack-21': '01560271717496.jpg', 'stack-22': '01560271790317.jpg',
    'stack-23': '01560271870737.jpg', 'stack-24': '01560271949372.jpg',
    'stack-25': '01560272030405.jpg'}

measurements = dict()
measurements['FastFourier'] = fastFourier
measurements['Laplacian'] = variance_of_laplacian
measurements['RingDiff'] = ringDiff
measurements['JpegSize'] = jpegSize
measurements['Sharpness'] = get_sharpness
measurements['BRISQUE'] = brisque_score


MEASUREMENT = 'Sharpness'

for folder in os.listdir(stacks240):
    wk_dir = os.path.join(stacks240, folder)
    if folder in real_focus_dict.keys():
        real_focus = os.path.join(wk_dir, real_focus_dict[folder])

        img_files = []
        for img_file_name in os.listdir(wk_dir):
            img_file = os.path.join(wk_dir, img_file_name)
            img_files.append(img_file)

        res = dict()
        print('Running bruteforce algorithm in {} with {} as measurement.'
        .format(folder, MEASUREMENT))

        count = 0
        zero_percent = True
        twenty_five_percent = True
        fifty_percent = True
        seventy_five_percent = True

        print("Calculating...")
        for img in img_files:
            count += 1
            preprocessed_img = preprocess_img(img)


            res[img] = measurements[MEASUREMENT](preprocessed_img)
            sorted_dict = {k: v for k, v in sorted(res.items(), key=lambda item: item[1])}
            loading = count / len(img_files)

            if loading < 0.25 and zero_percent:
                print("0%...")
                zero_percent = False

            elif loading >= 0.25 and twenty_five_percent:
                print("25%...")
                twenty_five_percent = False
            
            elif loading >= 0.50 and fifty_percent:
                print("50%...")
                fifty_percent = False

            elif loading >= 0.75 and seventy_five_percent:
                print("75%...")
                seventy_five_percent = False

        start_position = img_files.index(real_focus)

        score2 = sorted_dict[real_focus]

        position = list(sorted_dict.keys()).index(real_focus)
        print("The correct focus is image {} in {} position, with a score of {}."
        .format(real_focus, position, score2))
        print("The image is in the {}% percentile of the focal stack".format(
            start_position/len(res) * 100
        ))


